$(function () {
    loadSquad();
    loadNextGameOne();
    loadNextGameTwo();
    loadTabel();
})

function loadSquad() {
    $.ajax({
        url: "http://api.football-data.org/v2/teams/5",
        headers: {
            "x-auth-token": "c88ec5c8bf334b559bae89d559f4419b"
        },
        method: "GET",
        success: function(data) {
            var html = '<table style="color:white"> <th> Spieler </th> <th> Trikotnummer  </th>' 
            data.squad.forEach(element => {
                if(element.shirtNumber != null) {
                    html += '<tr> <td>' + element.name + '</td> <td style="padding-right:20px>'+ element.shirtNumber + '</td> </tr>'
                }
            });
            html += '</table>'
            $('#players').html(html); 
        },
        error: function (error) {
            alert(error);
        }
    })
}

function loadNextGameOne() {
    $.ajax({
        url: "http://api.football-data.org/v2/teams/5/matches/?status=SCHEDULED&limit=1",
        headers: {
            "x-auth-token": "c88ec5c8bf334b559bae89d559f4419b"
        },
        method: "GET",
        success: function(data) {
            var html = '';
            var datum = '';
            var datetime = '';
            var date = '';
            var year = '';
            var month = '';
            var day = '';
            var time = '';
            var hometeam = '';
            var awayteam = '';
            data.matches.forEach(element => {
                datetime = element.utcDate;
                year = datetime.slice(0, 4);
                month = datetime.slice(5, 7);
                day = datetime.slice(8, 10);
                time = datetime.slice(11, 16);
                date = day + '.' + month + '.' + year;
                datum = '<p> Datum: ' + date + ' ' + time + ' Uhr </p>'

                hometeam = '<p style="text-align:center">' + element.homeTeam.name + '</p>';
                awayteam = '<p style="text-align:center">' + element.awayTeam.name + '</p>';
                html = '<h2> Nächstes Spiel </h2>' + datum + '<br>' + hometeam + '<p style="text-align:center"> vs. </p>' + awayteam;
        });
            $('#nextGameOne').html(html);
        },
        error: function (error) {
            alert(error);
        }
    })
}

function loadNextGameTwo() {
    $.ajax({
        url: "http://api.football-data.org/v2/teams/5/matches/?status=SCHEDULED&limit=2",
        headers: {
            "x-auth-token": "c88ec5c8bf334b559bae89d559f4419b"
        },
        method: "GET",
        success: function(data) {
            var html = '';
            var datum = '';
            var datetime = '';
            var date = '';
            var year = '';
            var month = '';
            var day = '';
            var time = '';
            var hometeam = '';
            var awayteam = '';
            data.matches.forEach(element => {
                datetime = element.utcDate;
                year = datetime.slice(0, 4);
                month = datetime.slice(5, 7);
                day = datetime.slice(8, 10);
                time = datetime.slice(11, 16);
                date = day + '.' + month + '.' + year;
                datum = '<p> Datum: ' + date + ' ' + time + ' Uhr </p>'

                hometeam = '<p style="text-align:center">' + element.homeTeam.name + '</p>';
                awayteam = '<p style="text-align:center">' + element.awayTeam.name + '</p>';
                html = '<h2> Übernächstes Spiel </h2>' + datum + '<br>' + hometeam + '<p style="text-align:center"> vs. </p>' + awayteam;
        });
            $('#nextGameTwo').html(html);
        },
        error: function (error) {
            alert(error);
        }
    })
}

function loadTable() {
    $.ajax({
        url: "http://api.football-data.org/v2/teams/5",
        headers: {
            "x-auth-token": "c88ec5c8bf334b559bae89d559f4419b"
        },
        method: "GET",
        success: function(data) {
            var html = '<table style="color:white"> <th> Spieler </th> <th> Trikotnummer  </th>' 
            data.squad.forEach(element => {
                if(element.shirtNumber != null) {
                    html += '<tr> <td>' + element.name + '</td> <td style="padding-right:20px>'+ element.shirtNumber + '</td> </tr>'
                }
            });
            html += '</table>'
            $('#players').html(html); 
        },
        error: function (error) {
            alert(error);
        }
    })
}